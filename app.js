const bodyParser = require("body-parser");
const express = require('express')
const app = express()
const port = 3000;

const parser= require("body-parser")

// parse application/json
app.use(parser.json())

const jsonBody=parser.json()
app.get('/', (req, res) => {
  res.send('Ini Root Aplikasi');
});

app.get('/mahasiswa', (req, res) => {
    var mhs=["jajang", "ahmat", "asep"];
    res.send(mhs);
  })

app.post('/tambahpost', function (req, res) {
  res.send('Selamat Datang di Tambah Post')
});
  
app.post('/tambahpost2',jsonBody, (req, res) => {  
  res.json({
    nama: 'Panji Alpian Nurfadillah',
    email: 'panji.alpian007@gmail.com',
    noHP: '08995826903',
  });
});

app.put('/put', function (req, res) {
  res.send('PUT menggunakan method put')
});

app.put('/put2', function (req, res) {
  // res.json({
  //   nama: 'panji alpian',
  //   email: 'panji.alpian007@gmail.com',
  //   noHP: '08995826903',
  // });
  res.status(500).json({ error: 'message' })
});

app.delete('/delete', function (req, res) {
  res.send('DELETE menggunakan method delete')
});

app.listen(port, () => {
  console.log(`Example app listening on port ${port}`)
});
